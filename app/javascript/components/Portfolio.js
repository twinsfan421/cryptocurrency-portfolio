import React, { Component } from 'react'
import PortfolioItem from "./PortfolioItem"

class Portfolio extends Component {
    constructor(props){
        super(props)
    }
    
    render(){
        var amount = 0
        const portfolioItems = this.props.portfolioItems.map( (item, index) => {
            amount += item.value;
            return <PortfolioItem item={item} key={index} />
        })

        return(
            <div>
                <div className="portfolio-value">
                    <h1 className="portfolio-value--header">Your Total Portfolio Value Is:</h1>
                    <div className="portfolio-value--content">${amount}</div>
                </div>
                <div className="portfolio-items">
                    {portfolioItems}
                </div>
            </div>
        )
    }
}

export default Portfolio