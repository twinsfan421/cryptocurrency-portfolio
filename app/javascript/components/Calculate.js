import React, { Component } from 'react'

class Calculate extends Component {
    constructor(props){
        super(props)
    }

    render(){
        return(
            <div>
                <h1>Hom much do {this.props.active_currency.name} you own?</h1>
                <form onSubmit={this.props.handleSubmit}>
                    <div className="form-group">
                        <label>Enter amount owned</label><br/>
                        <input onChange={this.props.handleAmount} autoComplete="off" type="text" name="amount" placeholder="How much do you own?"
                         className="field"></input>
                    </div>
                    <div className="form-group">
                        <input type="submit" className="calculate-btn" value="caluclate My Total"></input>
                    </div>
                    <div >
                    </div>
                </form>
            </div>        
        )
    }
}

export default Calculate