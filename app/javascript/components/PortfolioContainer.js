import React, { Component } from 'react'
import Search from './Search'
import Calculate from './Calculate'
import Portfolio from './Portfolio'
import axios from 'axios'

class PortfolioContainer extends Component {
    constructor(props){
        super(props)
    

    this.state = {
        portfolio: [],
        search_results: [],
        active_currency: null,
        amount: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSelect = this.handleSelect.bind(this)
    this.handleAmount = this.handleAmount.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(e){

      axios.post('http://localhost:3000/search', {
          search: e.target.value
      })
      .then( (data) => {
        this.setState({
            search_results: [...data.data.currencies]
        })
  
      })
      .catch( (data) => {
        console.log(data)
      })
  }

  handleSubmit(e){
    e.preventDefault()

    let currency = this.state.active_currency.id
    let amount = this.state.amount


    axios.post('http://localhost:3000/calculate', {
        id: currency,
        amount: amount
    })
    .then( (data) => {
      this.setState({
          amount: '',
          active_currency: null,
          portfolio: [...this.state.portfolio, data.data]
      })
    })
    .catch( (data) => {
      console.log(data)
    })
}

  handleSelect(e){
    e.preventDefault()
    const id = e.currentTarget.value
    const activeCurrency = this.state.search_results.filter( item => item.id == parseInt(id))
    this.setState({
      active_currency: activeCurrency[0],
      search_results:[]
    })
  }

  handleAmount(e){
    this.setState({
      amount: e.target.value
    })
  }

  render(){

    return(
        <div className="grid">
          <div className="left">
            {this.state.active_currency ? 
            <Calculate 
            handleSubmit={this.handleSubmit}
            handleAmount={this.handleAmount}
            active_currency={this.state.active_currency}
            amount={this.state.amount}
            /> : 
            <Search handleSelect={this.handleSelect} 
            searchResults={this.state.search_results} 
            handleChange={this.handleChange} 
            />}
          </div>
          <div className="right">
            <Portfolio portfolioItems={this.state.portfolio}/>
          </div>
        </div>
    )
}

}

export default PortfolioContainer